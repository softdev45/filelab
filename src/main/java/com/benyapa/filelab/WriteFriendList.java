/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bwstx
 */
public class WriteFriendList {

    public static void main(String[] args) {
        ArrayList<Friend> friendList = new ArrayList<>();
        friendList.add(new Friend("Benyapa", 20, "0123456789"));
        friendList.add(new Friend("Suri", 20, "0223456789"));
        friendList.add(new Friend("Chathai", 21, "0312345678"));

        FileOutputStream fos = null;
        try {
            File file = new File("list_friends.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friendList);
            System.out.println(friendList);
            oos.close(); //always
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
